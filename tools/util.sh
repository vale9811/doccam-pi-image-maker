#!/bin/bash

function grey {
    echo -e "\e[249m$1"
}

function attention {
    printf "\e[36m$1\e[39m"
}

function error {
    printf "\e[31mERROR: $1\e[39m"
}

function printImportant {
    echo -e "\e[93m\e[4m$1\e[0m"
}

function printHeading {
    SECTION[0]=$(expr ${SECTION[0]} + 1)
    SECTION[1]=0

    echo
    printImportant "${SECTION[0]}: $1"
}

function printSubHeading {
    SECTION[1]=$(expr ${SECTION[1]} + 1)
    echo
    printImportant "${SECTION[0]}.${SECTION[1]}: $1"
}
