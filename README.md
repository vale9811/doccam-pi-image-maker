# Pi Image Creator
This is early days... use with caution.

This set of scripts creates a preconfigured raspi image for a 

Dependencies:
 - qemu
 - qemu-user-static
 - binfmt-support

U can use vagrant for that:
```ruby
Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/trusty64"
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update -y
    apt-get install -y qemu qemu-user-static binfmt-support
  SHELL
end
```


How to use:
 1. create a config.sh (look at config.sh.example)
	- if you feel like it, you can also pre-configure the doccam-pi config under `pi_files/doccam`
	- delete or encrypt `config.sh` after you're finished
 2. run `bash provision.sh` **as root**
 3. the image should now be located at `out/custom-pi.img` (or .tar.gz, look at https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3)
