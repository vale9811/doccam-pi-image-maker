#!/bin/bash
source config.sh

## Helpers
SECTION[0]=0
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd $DIR

if [ $(id -u) != "0" ]
then
    sudo ./"$0" "$@"  # Modified as suggested below.
    exit $?
fi

source tools/util.sh

function unmount_special {
    umount mnt/dev
    umount mnt/proc
    umount mnt/sys
}

function cleanup {
    printHeading "Cleaning Up"
    cd $DIR/$OUTDIR
    
    mv mnt/etc/resolv.conf.bak mnt/etc/resolv.conf
    rm mnt/usr/bin/qemu-arm-static
    rm -r mnt/tools
    rm -r mnt/pi_files
    rm mnt/chroot.sh
    rm mnt/config.sh
    unmount_special
    umount mnt/boot
    umount mnt
    losetup --detach "$LOOP"
}

function exit_if_error {
    if [ $? -eq 0 ]; then
	echo DONE
    else
	error "$1"
	cleanup
	exit 1
    fi
}

printHeading "Clean and Setup"
# clean up
if [ -d $OUTDIR ]; then
    while true; do
	read -p "Clean up previous attempts? [Y/N] " yn
	case $yn in
            [Yy]* ) rm -rf $OUTDIR; break;;
            [Nn]* ) break;;
            * ) echo "Please answer yes or no.";;
	esac
     done
fi

mkdir -p $OUTDIR
cd $OUTDIR

if [ ! -f image.tar.gz ]; then
    printHeading "Download the Image"
    wget -O image.tar.gz "$IMAGE"
    exit_if_error "Failed Downloading the Image"
fi

printHeading "Creating the Loopback Device"

printSubHeading "Allocating Space"
fallocate -l 2G custom-pi.img
exit_if_error "Could not allocate space."

printSubHeading "Formating the Device"
LOOP=$(losetup --find --show "custom-pi.img")
parted --script $LOOP mklabel msdos
parted --script $LOOP mkpart primary fat32 0% 100M
parted --script $LOOP mkpart primary ext4 100M 100%
exit_if_error "Formatting Failed."

printSubHeading "Creating File Systems"
PARTS=($(fdisk -l $LOOP | grep '^/dev' | cut -d' ' -f1))
if [ ! "${#PARTS[@]}" -eq "2"]; then
    error "Sth. wrong with the partitions."
    exit
fi

mkfs.vfat -F32 ${PARTS[0]}
mkfs.ext4 -F  ${PARTS[1]}
exit_if_error "Formatting Failed."

printHeading "Mounting the Loop Device"
mkdir mnt
mount ${PARTS[1]} mnt
mkdir mnt/boot
mount ${PARTS[0]} mnt/boot
exit_if_error "Mounting Failed"

printHeading "Extracting the Image"
tar -xpf image.tar.gz -C mnt 2> /dev/null
exit_if_error "Extracting Failed"

printHeading "Mounting System Devices"
mount -t proc none mnt/proc
mount -t sysfs none mnt/sys
mount -o bind /dev mnt/dev
exit_if_error "Mounting System Devices Failed"

printHeading "Setting up Misc Files"
mv mnt/etc/resolv.conf mnt/etc/resolv.conf.bak
cp /etc/resolv.conf mnt/etc/resolv.conf
cp $DIR/pi_files/boot/config.txt mnt/boot/config.txt
cp $DIR/pi_files/boot/config.txt mnt/boot/config.txt

printSubHeading "Setting up Qemu"
cp /usr/bin/qemu-arm-static mnt/usr/bin/
exit_if_error "Setting up Qemu Failed!"

printHeading "Executing Chroot"
cp $DIR/chroot.sh mnt/
cp $DIR/config.sh mnt/
cp -r $DIR/tools mnt/
cp -r $DIR/pi_files mnt/
chroot mnt ./chroot.sh

printHeading "Creating a TAR archive"
cd $DIR/$OUTDIR/mnt/

tar \
    --exclude="./proc/*" \
    --exclude=./tmp \
    --exclude=./dev \
    --exclude=./sys \
    --exclude=./run \
    -cpzf \
    ../custom-pi.tar.gz .

exit_if_error "Failing to create Image"

cleanup
