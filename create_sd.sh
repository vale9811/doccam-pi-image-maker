#!/bin/bash
source tools/util.sh
function cleanup {
    umount ${PARTS[0]}
    umount ${PARTS[1]}
}

function exit_if_error_c {
    if [ $? -eq 0 ]; then
	echo DONE
    else
	error "$1"
	cleanup
	exit 1
    fi
}


function exit_if_error {
    if [ $? -eq 0 ]; then
	echo DONE
    else
	error "$1"
	exit 1
    fi
}


DEV=$1
IMAGE=$2

while true; do
    read -p "Flash $IMAGE to $DEV? [Y/N] " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

MOUNT=$(mktemp -d)
MOUNT_BOOT=$(mktemp -d)

printHeading "Formatting the Disk"
parted --script $DEV mklabel msdos
exit_if_error
parted --script $DEV mkpart primary fat32 0% 100M
exit_if_error
parted --script $DEV mkpart primary ext4 100M 100%
exit_if_error

PARTS=($(fdisk -l $DEV | grep '^/dev' | cut -d' ' -f1))
if [ ! "${#PARTS[@]}" -eq "2" ]; then
    echo "Sth. wrong with the partitions."
    exit
fi

printHeading "Creating Filesystems"
mkfs.vfat -F32  ${PARTS[0]}
exit_if_error
mkfs.ext4 -F ${PARTS[1]}
exit_if_error

printHeading "Mounting"
mount ${PARTS[1]} $MOUNT
exit_if_error_c
mkdir -p $MOUNT/boot
mount ${PARTS[0]} $MOUNT_BOOT
exit_if_error_c

printHeading "Extracting"
tar -xpf $IMAGE -C $MOUNT
exit_if_error_c

mv $MOUNT/boot/* $MOUNT_BOOT

umount ${PARTS[0]}
umount ${PARTS[1]}
