#!/bin/bash
source /config.sh
source /tools/mo

# set-up keys
pacman-key --init
pacman-key --populate archlinuxarm
killall -9 gpg-agent

# install deps
pacman -Syu --noconfirm --ignore ca-certificates  --ignore ca-certificates-mozilla
pacman -Sy --noconfirm  --ignore ca-certificates  --ignore ca-certificates-mozilla nodejs ffmpeg git autossh sudo npm sshpass pacman-contrib

# users
echo "Lock root:"
passwd -l root
#echo -e "$ROOT_PW\n$ROOT_PW" | passwd

echo "Setting up cam user \'$CAM_USER\': "
useradd -m -s /usr/bin/bash $CAM_USER
useradd -m -s /usr/bin/bash $MAINTAIN_USER
usermod -aG wheel $MAINTAIN_USER
echo -e "$USER_PW\n$USER_PW" | passwd $MAINTAIN_USER
echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers

# set up cam stuff
mkdir -p $INSTALL_DIR
chown -R $CAM_USER $INSTALL_DIR

# clone and set-up
su $CAM_USER <<EOSU
git clone --depth=1 https://gitlab.com/vale9811/doccam-pi.git $INSTALL_DIR
cd $INSTALL_DIR
npm i
EOSU

# SSH setup
echo "none  /dev/pts  devpts  defaults 0 0" >> /etc/fstab
mount /dev/pts
su $CAM_USER <<EOSU
source /config.sh
mkdir -p ~/.ssh
cat >> ~/.ssh/config <<EOCAT
Host *
    StrictHostKeyChecking no
EOCAT
ssh-keygen -t rsa -b 4096 -C "" -N "" -f ~/.ssh/id_rsa
SSHPASS=$SSH_MASTER_PASSWORD sshpass -e ssh-copy-id $SSH_MASTER_USER@$SSH_MASTER -p $SSH_MASTER_PORT
EOSU

umount /dev/pts
head -n -1 /etc/fstab > /etc/fstab


# config
cd $INSTALL_DIR
cat /pi_files/doccam/config.js | mo > config.js
chown $CAM_USER config.js

# services
for SERVICE in /pi_files/systemd/*.service; do
    BNAME=$(basename $SERVICE)
    cat $SERVICE | mo > /etc/systemd/system/$BNAME
    systemctl enable $BNAME
done
systemctl enable sshd
systemctl enable paccache.timer

# power stuff / disable HDMI
echo "/usr/bin/tvservice -o" >> /etc/rc.local

cat /pi_files/ssh/sshd_config | mo > /etc/ssh/sshd_config

# clean
pacman -Scc --noconfirm
